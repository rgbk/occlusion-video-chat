// http://www.openframeworks.cc/tutorials/maths/001_vector_maths.html

vec2 offset_near = vec2(0,0); //a
uniform vec2 offset_far; //b
vec2 offset_delta = offset_far - offset_near; //c
int iterations = 8;
uniform int background_captured;

uniform sampler2DRect texture[3];

void main( void ) {
	
	// set canvas to transparent
	vec4 canvas = vec4(0,0,0,0);
	
	// start with the closest depths and fill in lesser depths in iterated calibrations
	for(int i=iterations; i>=0; i--){
		float last_step = (float(i)+1.35) / float(iterations);
		float step = float(i) / float(iterations);
		
		vec4 depthFragment = texture2DRect(texture[0], gl_TexCoord[0].st + (offset_near + (1.0-step) * offset_delta) );
		
		if( depthFragment[0] >= step && depthFragment[0] <= last_step && canvas[3] == 0.0 ){
			canvas = depthFragment;
		}
	}
	
	// render only non-hole data to the canvas
	// otherwise use a previously captured background plate
	
	vec4 old_canvas = texture2DRect(texture[1], gl_TexCoord[0].st);
	
	canvas[3] = 1.0;
	
	if(canvas[0] >= 0.01){
		old_canvas = canvas;
	}else if(background_captured == 1){
		vec4 backgroundFragment = texture2DRect(texture[2], gl_TexCoord[0].st);
		old_canvas = old_canvas*0.98 + backgroundFragment*0.02;
	}
	
	gl_FragColor = old_canvas;
}