uniform sampler2DRect texture[2];

void main( void ) {
	vec4 canvas;

	vec4 A = texture2DRect(texture[0], gl_TexCoord[0].st);
	vec4 B = texture2DRect(texture[1], gl_TexCoord[0].st);

	if(A[3] > B[3]){
		canvas = vec4(A[0],A[1],A[2],1.0);
	}else{
		canvas = vec4(B[0],B[1],B[2],1.0);
	}
	
	gl_FragColor = canvas;
}