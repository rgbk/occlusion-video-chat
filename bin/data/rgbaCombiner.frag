
uniform vec2 offset_near;
uniform float offset_near_scale;

uniform sampler2DRect texture[2];

void main( void ) {
	
	vec4 colorFragment = texture2DRect(texture[0], gl_TexCoord[0].st * offset_near_scale + offset_near);		
	vec4 depthFragment = texture2DRect(texture[1], gl_TexCoord[0].st);
	
	gl_FragColor = vec4(colorFragment[0], colorFragment[1], colorFragment[2], depthFragment[0]);
}