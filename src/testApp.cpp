#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup() {
	helpMode = false;
	ofToggleFullscreen();
	ofHideCursor();
	
	// standard window affair
	ofSetFrameRate(30);
	width = ofGetWindowWidth();
	height = ofGetWindowHeight();
	
	// device thingies
	kinect.init();
	kinect.setVerbose(false);
	kinect.open();

	// allocate memory for textures and GPU whatnot
	color_source = &kinect.getTextureReference();
	depth_source = &kinect.getDepthTextureReference();
	depth_cleaner.load(ofToDataPath("fullFrame.vert"), ofToDataPath("depthCleaner.frag"));
	depth_cleaned.allocate(640, 480, GL_RGBA);
	depth_background.allocate(640, 480);
	background_captured = false;
	
	// combines rgb and depth-as-alpha
	ofEnableAlphaBlending();
	rgba_combined.allocate(640,480,GL_RGBA);
	rgba_combiner.load(ofToDataPath("fullFrame.vert"), ofToDataPath("rgbaCombiner.frag"));

	// the magic that mixes it all together
	occluder.load(ofToDataPath("fullFrame.vert"), ofToDataPath("occluder.frag"));
	occlusion_canvas.allocate(640, 480, GL_RGB);
	
	// network stuff
	remoteCameraServer.init(640, 480, OF_IMAGE_COLOR_ALPHA);
	remoteCameraClient.initGrabber(640, 480, OF_IMAGE_COLOR_ALPHA,true);
	remoteCameraClient.start();
	
	cout << "reading SETTINGS ... ";
	if( xmlSettings.loadFile(ofToDataPath("settings.xml")) ){
//		cout << "SUCCESS" << endl;
		// near settings shift color
		offset_near.x = xmlSettings.getValue("offset_near_x", 62);
		offset_near.y = xmlSettings.getValue("offset_near_y", 29);
		offset_near_scale = xmlSettings.getValue("offset_near_scale", 0.922);
		// far settings shift depth in interpolated steps
		offset_far.x = xmlSettings.getValue("offset_far_x", 20);
		offset_far.y = xmlSettings.getValue("offset_far_y", 0);
		kinect_tilt = xmlSettings.getValue("kinect_tilt", 10);
		network_skip_frames = xmlSettings.getValue("network_skipframes", 0);
	}

	kinect.setCameraTiltAngle(kinect_tilt);
}

//--------------------------------------------------------------
void testApp::update() {

	// kinect things
	kinect.update();
	if(kinect.isFrameNew()) {
		ofSetColor(255,255,255);
		
		// clean that depth!!!
		// also shift the far pixels paralax-map-style!!!!
		depth_cleaned.begin();
		depth_cleaner.begin();
		depth_cleaner.setUniform1i("background_captured", background_captured?1:0);
		depth_cleaner.setUniform2f("offset_far", offset_far.x, offset_far.y);
		depth_cleaner.setUniformTexture("texture[0]", *depth_source, 0);
		depth_cleaner.setUniformTexture("texture[1]", depth_cleaned.getTextureReference(), 1);
		depth_cleaner.setUniformTexture("texture[2]", depth_background.getTextureReference(), 2);
		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(0, 0);	glVertex2f(0, 0);
		glTexCoord2f(0, height); glVertex2f(0, height);
		glTexCoord2f(width, 0); glVertex2f(width, 0);
		glTexCoord2f(width, height); glVertex2f(width, height);
		glEnd();
		depth_cleaner.end();
		depth_cleaned.end();
		
		// prep this machine's image for the (server) remote camera.
		// start using alpha as depth from this point forward
		rgba_combined.begin();
		ofBackground(0.0,0.0,0.0,0.0);
		rgba_combiner.begin();
		rgba_combiner.setUniform2f("offset_near", offset_near.x, offset_near.y);
		rgba_combiner.setUniform1f("offset_near_scale", offset_near_scale);
		rgba_combiner.setUniformTexture("texture[0]", *color_source, 0);
		rgba_combiner.setUniformTexture("texture[1]", depth_cleaned.getTextureReference(), 1);
		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(0, 0);	glVertex2f(0, 0);
		glTexCoord2f(0, height); glVertex2f(0, height);
		glTexCoord2f(width, 0); glVertex2f(width, 0);
		glTexCoord2f(width, height); glVertex2f(width, height);
		glEnd();
		rgba_combiner.end();
		rgba_combined.end();

		// send combined buffer to the server for delivery
		if(network_skip_frames_counter == 0){
			rgba_combined.getTextureReference().readToPixels(pixelData);
			remoteCameraServer.update(pixelData.getPixels());
			network_skip_frames_counter = network_skip_frames + 1;
		}
		network_skip_frames_counter--;
	}
	
	// (client) remote camera things
	remoteCameraClient.update();
		
	// occlude 'em!!!!!
	occlusion_canvas.begin();
	occluder.begin();
	occluder.setUniformTexture("texture[0]", rgba_combined.getTextureReference(), 0);
	occluder.setUniformTexture("texture[1]", remoteCameraClient.getTextureReference(), 1);
	glBegin(GL_TRIANGLE_STRIP);
	glTexCoord2f(0, 0);	glVertex2f(0, 0);
	glTexCoord2f(0, height); glVertex2f(0, height);
	glTexCoord2f(width, 0); glVertex2f(width, 0);
	glTexCoord2f(width, height); glVertex2f(width, height);
	glEnd();
	occluder.end();
	occlusion_canvas.end();

	
}

//--------------------------------------------------------------
void testApp::draw() {
	if(helpMode){
		ofBackground(0, 0, 0);
		ofSetColor(255,255,255);

		color_source->draw(0,0,200,150);
		ofDrawBitmapString("color source",0, 160);
		
		depth_source->draw(200,0,200,150);
		ofDrawBitmapString("depth source",200,160);
		
		depth_cleaned.draw(400,0,200,150);
		ofDrawBitmapString("depth cleaned",400,160);
		
		depth_background.draw(600,0,200,150);
		ofDrawBitmapString("depth background",600,160);
		
		remoteCameraClient.draw(800,0,200,150);
		ofDrawBitmapString("remote image",800,160);
		
		occlusion_canvas.draw(400,300,600,450);
		ofDrawBitmapString("occluded composite",400,290);
		
//		depth_cleaned.draw(0, 0, 400, 300);
//		depth_background.draw(400, 0, 400, 300);
//		occlusion_canvas.draw(400,300,400,300);
		
		// draw instructions
		ofSetColor(255, 0, 0);
		stringstream reportStream;
		reportStream << "accelerometer: "	<< ofToString(kinect.getMksAccel().x, 2) << " / "
											<< ofToString(kinect.getMksAccel().y, 2) << " / "
											<< ofToString(kinect.getMksAccel().z, 2) << "(" << kinect_tilt << ")" << endl
					 << "tilt: " << kinect_tilt << endl
					 << "machine fps: " << ofGetFrameRate() << endl
					 << "server fps: " << remoteCameraServer.getFPS() << endl
					 << "client fps: " << remoteCameraClient.getFPS() << endl
					 << "offset_near (u/o i/j/k/l): " << offset_near_scale << "x" << offset_near.x << "," << offset_near.y << endl
					 << "offset_far (w/a/s/d): " << offset_far.x << "," << offset_far.y << endl
					 << "B sets background: " << background_captured;
		ofDrawBitmapString(reportStream.str(),10,320);
	}else{
		ofSetColor(255,255,255);
		occlusion_canvas.draw(0,0,1024,768);
	}
}

//--------------------------------------------------------------
void testApp::exit() {
	kinect.close();
	remoteCameraServer.close();
	remoteCameraClient.close();
}

//--------------------------------------------------------------
void testApp::keyPressed (int key) {
	switch (key) {
			
		case OF_KEY_UP:
			kinect_tilt+=1;
			kinect.setCameraTiltAngle(kinect_tilt);
			break;
			
		case OF_KEY_DOWN:
			kinect_tilt-=1;
			kinect.setCameraTiltAngle(kinect_tilt);
			break;
			
		case 'f':
			ofToggleFullscreen();
			break;
			
		case 'h':
			helpMode = !helpMode;
			break;
			
		case 'b':
			depth_background.begin();
				ofSetColor(255,255,255);
				depth_cleaned.draw(0,0);
			depth_background.end();
			background_captured = !background_captured;
			break;
			
		case 'w'://up
			offset_far.y += 1;
			break;
			
		case 's'://down
			offset_far.y -= 1;
			break;
			
		case 'a'://left
			offset_far.x += 1;
			break;
			
		case 'd'://right
			offset_far.x -= 1;
			break;
			
		case 'u'://smaller
			offset_near_scale += 0.001;
			break;
			
		case 'o'://bigger
			offset_near_scale -= 0.001;
			break;
			
		case 'i'://up
			offset_near.y += 1;
			break;
			
		case 'k'://down
			offset_near.y -= 1;
			break;
			
		case 'j'://left
			offset_near.x += 1;
			break;
			
		case 'l'://right
			offset_near.x -= 1;
			break;
	}
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h)
{
	width = ofGetWindowWidth();
	height = ofGetWindowHeight();
}

