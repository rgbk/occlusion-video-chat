#pragma once

#include "ofMain.h"
#include "ofxKinect.h"
#include "ofxRemoteCameraServer.h"
#include "ofxRemoteCameraClient.h"
#include "ofxXmlSettings.h"

class testApp : public ofBaseApp {
public:
	bool helpMode;
	
	void setup();
	void update();
	void draw();
	void exit();

	void keyPressed(int key);

	void windowResized(int w, int h);
	int width, height;

	ofxKinect 		kinect;
	int				kinect_tilt;
	
	ofTexture*		color_source;
	
	ofTexture*		depth_source;
	ofShader		depth_cleaner;
	ofFbo			depth_cleaned;
	ofFbo			depth_background;
	bool			background_captured;

	ofVec2f			offset_far;
	float			offset_far_scale;
	ofVec2f			offset_near;
	float			offset_near_scale;
	
	ofFbo			rgba_combined;
	ofShader		rgba_combiner;

	ofShader		occluder;
	ofFbo			occlusion_canvas;
	
	int	network_skip_frames;
	int network_skip_frames_counter;
	
	// server stuff
	ofxRemoteCameraServer	remoteCameraServer;
	ofPixels				pixelData;
	
	// client stuff
	ofxRemoteCameraClient	remoteCameraClient;
	
	ofxXmlSettings	xmlSettings;
	
};
